package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Ship {
    private boolean isPlaced;
    private String name;
    private int size;
    private List<Position> positions;
    private Color color;

    public Ship() {
        this.positions = new ArrayList<>();
    }

    public Ship(String name, int size) {
        this();

        this.name = name;
        this.size = size;
    }

    public Ship(String name, int size, List<Position> positions) {
        this(name, size);

        this.positions = positions;
    }

    public Ship(String name, int size, Color color) {
        this(name, size);

        this.color = color;
    }

    public void addPosition(Position newPosition) {
        if (positions == null) {
            positions = new ArrayList<>();
        }
        positions.add(newPosition);
    }

    // TODO: property change listener implementieren

    public boolean isPlaced() {
        return isPlaced;
    }

    public void setPlaced(boolean placed) {
        isPlaced = placed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void applyHit(Position hitPosition) {
        for (Position position:positions) {
            if (position.equals(hitPosition)) {
                position.setIsHit(true);
            }
        }
    }

    public boolean isSunk() {
        for(Position position: positions) {
            if (!position.getIsHit()) {
               return false;
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        if (this.positions.size() == ship.positions.size()) {
            for (int i = 0; i < this.positions.size(); i++) {
                if (!this.positions.get(i).equals(ship.positions.get(i))) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return isPlaced == ship.isPlaced && size == ship.size && Objects.equals(name, ship.name) && color == ship.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isPlaced, name, size, positions, color);
    }
}
