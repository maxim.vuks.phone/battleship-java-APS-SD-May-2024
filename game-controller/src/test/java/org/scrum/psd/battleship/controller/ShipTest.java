package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

public class ShipTest {
    @Test
    public void shouldReturnTrueWhenShipIsSunk() {
        Ship ship = new Ship("TestShip", 3);
        ship.getPositions().add(new Position(Letter.A, 1));
        ship.getPositions().add(new Position(Letter.A, 2));
        ship.getPositions().add(new Position(Letter.A, 3));
        ship.applyHit(new Position(Letter.A, 1));
        ship.applyHit(new Position(Letter.A, 2));
        ship.applyHit(new Position(Letter.A, 3));

        Assert.assertTrue(ship.isSunk());
    }

    @Test
    public void shouldReturnFalseWhenShipIsNotSunk() {
        Ship ship = new Ship("TestShip", 3);
        ship.getPositions().add(new Position(Letter.A, 1));
        ship.getPositions().add(new Position(Letter.A, 2));
        ship.getPositions().add(new Position(Letter.A, 3));
        ship.applyHit(new Position(Letter.A, 1));
        ship.applyHit(new Position(Letter.A, 2));

        Assert.assertFalse(ship.isSunk());
    }

    @Test
    public void shouldReturnTrueWhenPositionIsHit() {
        Ship ship = new Ship("TestShip", 3);
        Position position = new Position(Letter.A, 1);
        ship.getPositions().add(position);
        ship.applyHit(position);

        Assert.assertTrue(position.getIsHit());
    }

    @Test
    public void shouldReturnFalseWhenPositionIsNotHit() {
        Ship ship = new Ship("TestShip", 3);
        Position position = new Position(Letter.A, 1);
        ship.getPositions().add(position);

        Assert.assertFalse(position.getIsHit());
    }
}