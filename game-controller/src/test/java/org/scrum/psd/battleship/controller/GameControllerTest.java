package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;
import java.util.List;

public class GameControllerTest {
    @Test(expected = RuntimeException.class)
    public void shouldThrowExceptionWhenShipNotFound() {
        List<Ship> ships = GameController.initializeShips();
        Position position = new Position(Letter.H, 10);

        GameController.getShip(ships, position);
    }

    @Test
    public void shouldReturnShipWhenPositionMatches() {
        List<Ship> ships = GameController.initializeShips();
        Position position = new Position(Letter.A, 1);
        ships.get(0).getPositions().add(position);

        Ship result = GameController.getShip(ships, position);

        Assert.assertEquals(ships.get(0), result);
    }

    @Test
    public void testCheckIsHitTrue() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.A, 1));

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckIsHitFalse() {
        List<Ship> ships = GameController.initializeShips();
        int counter = 0;

        for (Ship ship : ships) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(ships, new Position(Letter.H, 1));

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitPositionIsNull() {
        GameController.checkIsHit(GameController.initializeShips(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitShipIsNull() {
        GameController.checkIsHit(null, new Position(Letter.H, 1));
    }

    @Test
    public void testIsShipValidFalse() {
        Ship ship = new Ship("TestShip", 3);
        boolean result = GameController.isShipValid(ship);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsShipValidTrue() {
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 1), new Position(Letter.A, 1));
        Ship ship = new Ship("TestShip", 3, positions);

        boolean result = GameController.isShipValid(ship);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnTrueWhenNewPositionsAreValidDown() {
        List<Ship> fleet = GameController.initializeShips();
        List<Position> newPositions = Arrays.asList(new Position(Letter.H, 1), new Position(Letter.H, 2));

        boolean result = GameController.validateNewPositions(fleet, newPositions);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnTrueWhenNewPositionsAreValidRight() {
        List<Ship> fleet = GameController.initializeShips();
        List<Position> newPositions = Arrays.asList(new Position(Letter.E, 1), new Position(Letter.F, 1));

        boolean result = GameController.validateNewPositions(fleet, newPositions);

        Assert.assertTrue(result);
    }

    @Test
    public void shouldReturnFalseWhenNewPositionsAreInvalid() {
        List<Ship> fleet = GameController.initializeShips();
        fleet.get(0).getPositions().add(new Position(Letter.A, 1));
        List<Position> newPositions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.H, 2));

        boolean result = GameController.validateNewPositions(fleet, newPositions);

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenNewPositionsAreNull() {
        GameController.validateNewPositions(GameController.initializeShips(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenFleetIsNull() {
        GameController.validateNewPositions(null, Arrays.asList(new Position(Letter.A, 1), new Position(Letter.H, 2)));
    }
}
