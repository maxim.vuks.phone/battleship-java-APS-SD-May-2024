package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcolor.Attribute;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.*;

import static com.diogonunes.jcolor.Ansi.colorize;
import static com.diogonunes.jcolor.Attribute.*;

public class Main {
    private static List<Ship> myFleet;
    private static List<Ship> enemyFleet;
    private static final Set<Position> enemyPreviousShots = new HashSet<>();

    private static final Telemetry telemetry = new Telemetry();

    public static void main(String[] args) {
        telemetry.trackEvent("ApplicationStarted", "Technology", "Java");
        System.out.println(colorize("                                     |__", MAGENTA_TEXT()));
        System.out.println(colorize("                                     |\\/", MAGENTA_TEXT()));
        System.out.println(colorize("                                     ---", MAGENTA_TEXT()));
        System.out.println(colorize("                                     / | [", MAGENTA_TEXT()));
        System.out.println(colorize("                              !      | |||", MAGENTA_TEXT()));
        System.out.println(colorize("                            _/|     _/|-++'", MAGENTA_TEXT()));
        System.out.println(colorize("                        +  +--|    |--|--|_ |-", MAGENTA_TEXT()));
        System.out.println(colorize("                     { /|__|  |/\\__|  |--- |||__/", MAGENTA_TEXT()));
        System.out.println(colorize("                    +---------------___[}-_===_.'____                 /\\", MAGENTA_TEXT()));
        System.out.println(colorize("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _", MAGENTA_TEXT()));
        System.out.println(colorize(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7", MAGENTA_TEXT()));
        System.out.println(colorize("|                        Welcome to Battleship                         BB-61/", MAGENTA_TEXT()));
        System.out.println(colorize(" \\_________________________________________________________________________|", MAGENTA_TEXT()));
        System.out.println();

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\033[2J\033[;H");
        System.out.println("                  __");
        System.out.println("                 /  \\");
        System.out.println("           .-.  |    |");
        System.out.println("   *    _.-'  \\  \\__/");
        System.out.println("    \\.-'       \\");
        System.out.println("   /          _/");
        System.out.println("  |      _  /\" \"");
        System.out.println("  |     /_\'");
        System.out.println("   \\    \\_/");
        System.out.println("    \" \"\" \"\" \"\" \"");

        do {
            System.out.println();
            System.out.println(colorize("========================================================================", BOLD()));
            System.out.println(colorize("Player, it's your turn", GREEN_TEXT()));
            System.out.println(colorize("Enter coordinates for your shot:", GREEN_TEXT()));
            Position position = parsePosition(scanner.next());
            boolean isHit = GameController.checkIsHit(enemyFleet, position);
            if (isHit) {
                showHitGraphics();
                System.out.println(colorize("Yeah ! Nice hit !", BRIGHT_RED_TEXT()));
                Ship shipThatWasHit = GameController.getShip(enemyFleet, position);
                shipThatWasHit.applyHit(position);
                if (shipThatWasHit.isSunk()) {
                    System.out.println();
                    System.out.println(colorize("---------------------", YELLOW_TEXT()));
                    System.out.println("THE SHIP IS DESTROYED");
                    System.out.println("Enemy fleet stats");
                    showFleetStats(enemyFleet);
                }
            } else {
                System.out.println(colorize("Miss", BRIGHT_BLUE_TEXT()));
                showMissGraphics();
            }
            telemetry.trackEvent("Player_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            if (fleetSunk(enemyFleet)) {
                System.out.println("You won!");
                break;
            }
            position = getRandomPosition(enemyPreviousShots);
            isHit = GameController.checkIsHit(myFleet, position);
            System.out.println();

            System.out.println(colorize("---------------------------------------------------------------------", BOLD()));
            System.out.print(colorize(String.format("Computer shoot in %s%s and ", position.getColumn(), position.getRow()), MAGENTA_TEXT()));
            if (isHit) {
                System.out.println(colorize("hit your ship !", BRIGHT_RED_TEXT()));
            } else {
                System.out.println(colorize("miss", BRIGHT_BLUE_TEXT()));
                showMissGraphics();
            }
            telemetry.trackEvent("Computer_ShootPosition", "Position", position.toString(), "IsHit", Boolean.valueOf(isHit).toString());
            if (isHit) {
                showHitGraphics();
                Ship shipThatWasHit = GameController.getShip(myFleet, position);
                shipThatWasHit.applyHit(position);
                if (shipThatWasHit.isSunk()) {
                    System.out.println();
                    System.out.println("ENEMY SUNK YOUR SHIP!");
                    System.out.println("Player fleet stats");
                    showFleetStats(myFleet);
                }
            }
            if (fleetSunk(myFleet)) {
                System.out.println("Computer won!");
                break;
            }
        } while (true);
    }

    private static boolean fleetSunk(List<Ship> fleet) {
        for (Ship ship : fleet) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;
    }

    private static void showFleetStats(List<Ship> fleet) {
        for (Ship ship : fleet) {
            Attribute textAttribute = ship.isSunk() ? BRIGHT_RED_TEXT() : GREEN_TEXT();
            for (int i = 0; i < ship.getSize(); i++) {
                System.out.print(colorize("X", textAttribute));
            }
            System.out.print(" ");
        }
        System.out.println();
    }

    private static void showHitGraphics() {
        beep();

        System.out.println(colorize("                \\         .  ./", BRIGHT_RED_TEXT()));
        System.out.println(colorize("              \\      .:\" \";'.:..\" \"   /,", BRIGHT_RED_TEXT()));
        System.out.println(colorize("                  (M^^.^~~:.'\" \").", BRIGHT_RED_TEXT()));
        System.out.println(colorize("            -   (/  .    . . \\ \\)  -", BRIGHT_RED_TEXT()));
        System.out.println(colorize("               ((| :. ~ ^  :. .|))", BRIGHT_RED_TEXT()));
        System.out.println(colorize("            -   (\\- |  \\ /  |  /)  -", BRIGHT_RED_TEXT()));
        System.out.println(colorize("                 -\\  \\     /  /-", BRIGHT_RED_TEXT()));
        System.out.println(colorize("                   \\  \\   /  /", BRIGHT_RED_TEXT()));
    }

    private static void showMissGraphics() {
        System.out.println(colorize("                \\           ", BRIGHT_BLUE_TEXT()));
        System.out.println(colorize("              \\      .:\" \";'", BRIGHT_BLUE_TEXT()));
        System.out.println(colorize("                  (M^^.^~~:.'\" ", BRIGHT_BLUE_TEXT()));
        System.out.println(colorize("            -   (/  .    . . \\ \\)  -", BRIGHT_BLUE_TEXT()));
        System.out.println(colorize("               ((| :. ~ ^  :. .|))", BRIGHT_BLUE_TEXT()));

    }

    private static void beep() {
        System.out.print("\007");
    }

    protected static Position parsePosition(String input) {
        Letter letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
        int number = Integer.parseInt(input.substring(1));
        return new Position(letter, number);
    }

    public static Position getRandomPosition(Set<Position> previousShots) {
        int rows = 8;
        int lines = 8;
        Position randomPosition;
        do {
            Random random = new Random();
            Letter letter = Letter.values()[random.nextInt(lines)];
            int number = random.nextInt(rows) + 1;
            randomPosition = new Position(letter, number);
        } while (previousShots.contains(randomPosition));
        previousShots.add(randomPosition);
        return randomPosition;
    }

    private static void InitializeGame() {
        InitializeMyFleet();
        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initializeShips();

        System.out.println("Please position your fleet (Game board has size from A to H and 1 to 8):");

        for (Ship ship : myFleet) {
            // Calculate positions based on the direction and add them to the ship
            boolean shipPlacedCorrectly = false;
            List<Position> positions = new LinkedList<>();
            do {
                System.out.printf("\nPlease enter the starting position for the %s (size: %d) (e.g., A3):%n", ship.getName(), ship.getSize());
                String startInput = scanner.next();

                System.out.printf("Enter direction for the %s (D for down, R for right):%n", ship.getName());
                String direction = scanner.next();
                try {
                    positions = BoardGenerator.calculatePositions(startInput, direction.toUpperCase(), ship.getSize());
                    shipPlacedCorrectly = true;
                } catch (IndexOutOfBoundsException e) {
                    System.out.println(colorize("You tried to place the ship outside of the board", BRIGHT_RED_TEXT()));
                }

                if (!GameController.validateNewPositions(myFleet, positions)) {
                    System.out.println(colorize("Ship overlaps previously placed ships, try again", BRIGHT_RED_TEXT()));
                    shipPlacedCorrectly = false;
                }
            } while (!shipPlacedCorrectly);

            for (Position position : positions) {
                ship.addPosition(position);
            }

            // Assuming telemetry is setup for event tracking
            for (int i = 0; i < positions.size(); i++) {
                telemetry.trackEvent("Player_PlaceShipPosition", "Position", positions.get(i).toString(), "Ship", ship.getName(), "PositionInShip", String.valueOf(i + 1));

            }
        }
    }


    private static void InitializeEnemyFleet() {
        enemyFleet = GameController.initializeShips();
        int boardNumber = (int) (Math.random() * 5);
        BoardGenerator.generateBoard(enemyFleet, boardNumber);
    }
}
