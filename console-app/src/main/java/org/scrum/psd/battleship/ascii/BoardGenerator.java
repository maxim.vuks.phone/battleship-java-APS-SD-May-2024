package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.LinkedList;
import java.util.List;

public class BoardGenerator {
    static void generateBoard(List<Ship> fleet, int variation) {
        switch (variation) {
            case 0:
                fleet.get(0).getPositions().add(new Position(Letter.C, 8));
                fleet.get(0).getPositions().add(new Position(Letter.D, 8));
                fleet.get(0).getPositions().add(new Position(Letter.E, 8));
                fleet.get(0).getPositions().add(new Position(Letter.F, 8));
                fleet.get(0).getPositions().add(new Position(Letter.G, 8));

                fleet.get(1).getPositions().add(new Position(Letter.B, 4));
                fleet.get(1).getPositions().add(new Position(Letter.B, 5));
                fleet.get(1).getPositions().add(new Position(Letter.B, 3));
                fleet.get(1).getPositions().add(new Position(Letter.B, 6));

                fleet.get(2).getPositions().add(new Position(Letter.C, 1));
                fleet.get(2).getPositions().add(new Position(Letter.D, 1));
                fleet.get(2).getPositions().add(new Position(Letter.E, 1));

                fleet.get(3).getPositions().add(new Position(Letter.H, 2));
                fleet.get(3).getPositions().add(new Position(Letter.H, 3));
                fleet.get(3).getPositions().add(new Position(Letter.H, 4));

                fleet.get(4).getPositions().add(new Position(Letter.E, 4));
                fleet.get(4).getPositions().add(new Position(Letter.E, 5));
            case 1:
                fleet.get(0).getPositions().add(new Position(Letter.A, 2));
                fleet.get(0).getPositions().add(new Position(Letter.A, 3));
                fleet.get(0).getPositions().add(new Position(Letter.A, 4));
                fleet.get(0).getPositions().add(new Position(Letter.A, 5));
                fleet.get(0).getPositions().add(new Position(Letter.A, 6));

                fleet.get(1).getPositions().add(new Position(Letter.D, 1));
                fleet.get(1).getPositions().add(new Position(Letter.E, 1));
                fleet.get(1).getPositions().add(new Position(Letter.F, 1));
                fleet.get(1).getPositions().add(new Position(Letter.G, 1));

                fleet.get(2).getPositions().add(new Position(Letter.H, 4));
                fleet.get(2).getPositions().add(new Position(Letter.H, 5));
                fleet.get(2).getPositions().add(new Position(Letter.H, 6));

                fleet.get(3).getPositions().add(new Position(Letter.E, 8));
                fleet.get(3).getPositions().add(new Position(Letter.F, 8));
                fleet.get(3).getPositions().add(new Position(Letter.G, 8));

                fleet.get(4).getPositions().add(new Position(Letter.A, 8));
                fleet.get(4).getPositions().add(new Position(Letter.B, 8));
                break;
            case 2:
                fleet.get(0).getPositions().add(new Position(Letter.B, 2));
                fleet.get(0).getPositions().add(new Position(Letter.B, 3));
                fleet.get(0).getPositions().add(new Position(Letter.B, 4));
                fleet.get(0).getPositions().add(new Position(Letter.B, 5));
                fleet.get(0).getPositions().add(new Position(Letter.B, 6));

                fleet.get(1).getPositions().add(new Position(Letter.A, 8));
                fleet.get(1).getPositions().add(new Position(Letter.B, 8));
                fleet.get(1).getPositions().add(new Position(Letter.C, 8));
                fleet.get(1).getPositions().add(new Position(Letter.D, 8));

                fleet.get(2).getPositions().add(new Position(Letter.F, 1));
                fleet.get(2).getPositions().add(new Position(Letter.G, 1));
                fleet.get(2).getPositions().add(new Position(Letter.H, 1));

                fleet.get(3).getPositions().add(new Position(Letter.H, 6));
                fleet.get(3).getPositions().add(new Position(Letter.H, 7));
                fleet.get(3).getPositions().add(new Position(Letter.H, 8));

                fleet.get(4).getPositions().add(new Position(Letter.D, 1));
                fleet.get(4).getPositions().add(new Position(Letter.D, 2));
                break;
            case 3:
                fleet.get(0).getPositions().add(new Position(Letter.D, 1));
                fleet.get(0).getPositions().add(new Position(Letter.E, 1));
                fleet.get(0).getPositions().add(new Position(Letter.F, 1));
                fleet.get(0).getPositions().add(new Position(Letter.G, 1));
                fleet.get(0).getPositions().add(new Position(Letter.H, 1));

                fleet.get(1).getPositions().add(new Position(Letter.E, 8));
                fleet.get(1).getPositions().add(new Position(Letter.F, 8));
                fleet.get(1).getPositions().add(new Position(Letter.G, 8));
                fleet.get(1).getPositions().add(new Position(Letter.H, 8));

                fleet.get(2).getPositions().add(new Position(Letter.A, 6));
                fleet.get(2).getPositions().add(new Position(Letter.A, 7));
                fleet.get(2).getPositions().add(new Position(Letter.A, 8));

                fleet.get(3).getPositions().add(new Position(Letter.A, 2));
                fleet.get(3).getPositions().add(new Position(Letter.A, 3));
                fleet.get(3).getPositions().add(new Position(Letter.A, 4));

                fleet.get(4).getPositions().add(new Position(Letter.H, 3));
                fleet.get(4).getPositions().add(new Position(Letter.H, 4));
                break;
            case 4:
                fleet.get(0).getPositions().add(new Position(Letter.H, 1));
                fleet.get(0).getPositions().add(new Position(Letter.H, 2));
                fleet.get(0).getPositions().add(new Position(Letter.H, 3));
                fleet.get(0).getPositions().add(new Position(Letter.H, 4));
                fleet.get(0).getPositions().add(new Position(Letter.H, 5));

                fleet.get(1).getPositions().add(new Position(Letter.C, 8));
                fleet.get(1).getPositions().add(new Position(Letter.D, 8));
                fleet.get(1).getPositions().add(new Position(Letter.E, 8));
                fleet.get(1).getPositions().add(new Position(Letter.F, 8));

                fleet.get(2).getPositions().add(new Position(Letter.B, 6));
                fleet.get(2).getPositions().add(new Position(Letter.C, 6));
                fleet.get(2).getPositions().add(new Position(Letter.D, 6));

                fleet.get(3).getPositions().add(new Position(Letter.B, 4));
                fleet.get(3).getPositions().add(new Position(Letter.C, 4));
                fleet.get(3).getPositions().add(new Position(Letter.D, 4));

                fleet.get(4).getPositions().add(new Position(Letter.H, 7));
                fleet.get(4).getPositions().add(new Position(Letter.H, 8));
                break;
            default:
                throw new IllegalArgumentException("Generator supports only variations 0-4");
        }
    }

    static List<Position> calculatePositions(String startInput, String direction, int size) {
        char startCol = startInput.charAt(0);
        int startRow = Integer.parseInt(startInput.substring(1));
        List<Position> positions = new LinkedList<>();

        for (int i = 0; i < size; i++) {
            switch (direction) {
                case "D":
                    positions.add(Position.fromString("" + startCol + (startRow + i)));
                    if (startRow + i > 8) {
                        throw new IndexOutOfBoundsException("Ship is outside of the board");
                    }
                    break;
                case "R":
                    positions.add(Position.fromString("" + (char) (startCol + i) + startRow));
                    if ((char) (startCol + i) == 'I') {
                        throw new IndexOutOfBoundsException("Ship is outside of the board");
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Invalid direction. Use D, R.");
            }
        }
        return positions;
    }


}
