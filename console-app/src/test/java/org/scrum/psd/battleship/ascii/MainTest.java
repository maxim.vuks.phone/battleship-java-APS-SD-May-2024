package org.scrum.psd.battleship.ascii;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;

import java.util.HashSet;
import java.util.Set;

@Execution(ExecutionMode.CONCURRENT)
public class MainTest {

    @Test
    public void testParsePosition() {
        Position actual = Main.parsePosition("A1");
        Position expected = new Position(Letter.A, 1);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testParsePosition2() {
        //given
        Position expected = new Position(Letter.B, 1);
        //when
        Position actual = Main.parsePosition("B1");
        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void randomPositionShouldNeverHasRow0() {
        for (int i = 0; i < 1000; i++) {
            Assertions.assertNotEquals(0, Main.getRandomPosition(new HashSet<Position>()).getRow());
        }
    }

    @Test
    public void randomPositionShouldSometimeHasRow8() {
        Set<Position> positions = new HashSet<>();
        boolean hadRow8 = false;
        for (int i = 0; i < 1000; i++) {
            if(Main.getRandomPosition(positions).getRow() == 8) {
                hadRow8 = true;
                break;
            }
        }
        Assertions.assertTrue(hadRow8);
    }

    @Test
    public void shouldNotShootInPreviouslyTargetedCells() {
        // given all cells are previously targeted except for "E2"
        Set<Position> prevShots = new HashSet<>();
        prevShots.add(new Position(Letter.A, 1));
        prevShots.add(new Position(Letter.A, 2));
        prevShots.add(new Position(Letter.A, 3));
        prevShots.add(new Position(Letter.A, 4));
        prevShots.add(new Position(Letter.A, 5));
        prevShots.add(new Position(Letter.A, 6));
        prevShots.add(new Position(Letter.A, 7));
        prevShots.add(new Position(Letter.A, 8));
        prevShots.add(new Position(Letter.B, 1));
        prevShots.add(new Position(Letter.B, 2));
        prevShots.add(new Position(Letter.B, 3));
        prevShots.add(new Position(Letter.B, 4));
        prevShots.add(new Position(Letter.B, 5));
        prevShots.add(new Position(Letter.B, 6));
        prevShots.add(new Position(Letter.B, 7));
        prevShots.add(new Position(Letter.B, 8));
        prevShots.add(new Position(Letter.C, 1));
        prevShots.add(new Position(Letter.C, 2));
        prevShots.add(new Position(Letter.C, 3));
        prevShots.add(new Position(Letter.C, 4));
        prevShots.add(new Position(Letter.C, 5));
        prevShots.add(new Position(Letter.C, 6));
        prevShots.add(new Position(Letter.C, 7));
        prevShots.add(new Position(Letter.C, 8));
        prevShots.add(new Position(Letter.D, 1));
        prevShots.add(new Position(Letter.D, 2));
        prevShots.add(new Position(Letter.D, 3));
        prevShots.add(new Position(Letter.D, 4));
        prevShots.add(new Position(Letter.D, 5));
        prevShots.add(new Position(Letter.D, 6));
        prevShots.add(new Position(Letter.D, 7));
        prevShots.add(new Position(Letter.D, 8));
        prevShots.add(new Position(Letter.E, 1));
        prevShots.add(new Position(Letter.E, 2));
        prevShots.add(new Position(Letter.E, 3));
        // prevShots.add(new Position(Letter.E, 4));
        prevShots.add(new Position(Letter.E, 5));
        prevShots.add(new Position(Letter.E, 6));
        prevShots.add(new Position(Letter.E, 7));
        prevShots.add(new Position(Letter.E, 8));
        prevShots.add(new Position(Letter.F, 1));
        prevShots.add(new Position(Letter.F, 2));
        prevShots.add(new Position(Letter.F, 3));
        prevShots.add(new Position(Letter.F, 4));
        prevShots.add(new Position(Letter.F, 5));
        prevShots.add(new Position(Letter.F, 6));
        prevShots.add(new Position(Letter.F, 7));
        prevShots.add(new Position(Letter.F, 8));
        prevShots.add(new Position(Letter.G, 1));
        prevShots.add(new Position(Letter.G, 2));
        prevShots.add(new Position(Letter.G, 3));
        prevShots.add(new Position(Letter.G, 4));
        prevShots.add(new Position(Letter.G, 5));
        prevShots.add(new Position(Letter.G, 6));
        prevShots.add(new Position(Letter.G, 7));
        prevShots.add(new Position(Letter.G, 8));
        prevShots.add(new Position(Letter.H, 1));
        prevShots.add(new Position(Letter.H, 2));
        prevShots.add(new Position(Letter.H, 3));
        prevShots.add(new Position(Letter.H, 4));
        prevShots.add(new Position(Letter.H, 5));
        prevShots.add(new Position(Letter.H, 6));
        prevShots.add(new Position(Letter.H, 7));
        prevShots.add(new Position(Letter.H, 8));
        // when
        Position position = Main.getRandomPosition(prevShots);
        // then
        Assertions.assertEquals(Letter.E, position.getColumn());
        Assertions.assertEquals(4, position.getRow());

    }
}
