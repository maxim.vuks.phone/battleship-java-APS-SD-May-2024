package org.scrum.psd.battleship.ascii;

import org.junit.Assert.*;
import org.junit.Test;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BoardGeneratorTest {
    @Test
    public void shouldReturnDifferentBoards() {
        List<Ship> fleet1 = GameController.initializeShips();
        List<Ship> fleet2 = GameController.initializeShips();

        BoardGenerator.generateBoard(fleet1, 0);
        BoardGenerator.generateBoard(fleet2, 1);

        assertNotEquals(fleet2, fleet1);
    }

    @Test
    public void testCalculatePositionsDown() {
        List<String> expected = Arrays.asList("A1", "A2", "A3", "A4");
        assertEquals(expected, BoardGenerator.calculatePositions("A1", "D", 4));
    }

    @Test
    public void testCalculatePositionsRight() {
        List<String> expected = Arrays.asList("A1", "B1", "C1", "D1");
        assertEquals(expected, BoardGenerator.calculatePositions("A1", "R", 4));
    }

    @Test
    public void testCalculatePositionsWithInvalidDirection() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                BoardGenerator.calculatePositions("A1", "U", 4)
        );
        assertTrue(exception.getMessage().contains("Invalid direction"));
    }

    @Test
    public void testCalculatePositionsOutOfBounds() {
        // Assuming the board ends at 'H' for columns and '8' for rows
        List<String> resultDown = BoardGenerator.calculatePositions("A6", "D", 4);
        assertTrue(resultDown.containsAll(Arrays.asList("A6", "A7", "A8")));

        List<String> resultRight = BoardGenerator.calculatePositions("G1", "R", 4);
        assertTrue(resultRight.containsAll(Arrays.asList("G1", "H1")));
    }

    // Optional: If your game should handle errors for out-of-bounds positions
    @Test
    public void testInvalidCalculatePositionsOutOfBounds() {
        assertThrows(IndexOutOfBoundsException.class, () ->
                BoardGenerator.calculatePositions("A8", "D", 3)
        );
        assertThrows(IndexOutOfBoundsException.class, () ->
                BoardGenerator.calculatePositions("H1", "R", 3)
        );
    }
}
